> Note : Je m'intéresse avant tout aux logiciels libres et open source.
> Diagrammes sur https://app.diagrams.net.

# Urbanisation de Système d'Information

## Introduction sur l'urbanisation des Systèmes d'Information et le méta-modèle de Christophe Longépé

L'Urbanisation de Système d'Information est un métier à part entière. Généralement réalisé par des Architectes du SI, l'urbanisation du système d'information a pour principal client le responsable du SI et la direction générale. 

Le principal enjeu de l'urbanisation d'un SI est (`j'adore cette formule :)`) l'alignement stratégique du système d'information, en fait « l'informatique » sur les métiers de l'entreprise (ou de l'entité). Et quand on parle « d'alignement sur les métiers », on parle plus de « résultats opérationnels ».

L'urbanisation de SI est aussi un très estimable outil pour l'animation du département SI, ses métiers et ses équipes.

Ici, il ne s'agit donc pas de faire de l'Urba de SI mais juste d'utiliser le méta-modèle de Longépé pour mieux communiquer sur l'organisation du système d'information d'une entité comme une association. 

## Les quatre niveaux du modèle de Longépé

Pour avoir une vision pratique d'un système d'information et surtout une vision partagé par tous, il faut utiliser ce méta-modèle.

Ce modèle contien quatre couches : 
1. Le niveau métier : les processus, la vision stratégique de l'entité, 
2. Le niveau fonctionnel,
3. Le niveau des applications,
4. Le niveau technique, les serveurs, les connexions, etc.

Je ne suis pas expert du sujet, je ne prétends pas appliquer le modèle à la lettre. Je l'applique dans le but de mieux partager avec les utilisateurs finaux.


## Exemple d'application avec la « comptabilité » d'une association basée sur le logiciel Garradin

Un exemple d'application, issu de mon interprétation :), du méta-modèle de Christophe Longépé.

![Méta-modèle Longépé appliqué à la comptabilité d'une association](https://gitlab.com/jrd10/urbanisation-si/-/raw/master/modeles/Urba-SI-Anonyme_Comptabilite-Sauvegarde_v01.png "Diagramme sur Diagrams.net. En CC0")


## Licence

Dans le domaine publique : CC0
